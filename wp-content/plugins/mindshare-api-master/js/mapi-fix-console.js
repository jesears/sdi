/**
 *
 * @created 3/21/13 1:37 PM
 * @author Mindshare Studios, Inc.
 * @copyright Copyright (c) 2006-2015
 * @link https://mindsharelabs.com/downloads/mindshare-theme-api/
 *
 */
if(!window.console){(function(){var B=["log","debug","info","warn","error","assert","dir","dirxml","group","groupEnd","time","timeEnd","count","trace","profile","profileEnd"];window.console={};for(var A=0;A<B.length;++A){window.console[B[A]]=function(){};}}());}
