<?php
/**
 * Template Name: Landing Page
 */
get_header(); ?>

<div class="container">

	<div class="row">

			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

					<section class="entry-content clearfix" itemprop="articleBody">
						<?php the_content(); ?>
					</section>
					<section class="entry-content no-padding clearfix">
						<?php include (get_stylesheet_directory()).'/inc/landing-page-links.php'; ?>
					</section>

					<footer class="article-footer">
						<?php
						wp_link_pages(
								array(
										'next_or_number' => 'number',
										'nextpagelink' => __('Next page', 'blankout'),
										'previouspagelink' => __('Previous page', 'blankout'),
										'pagelink' => '%',
										'echo' => 1
								)
						);
						?>
						<?php if(function_exists('mapi_edit_link')) {
							echo mapi_edit_link();
						} ?>
					</footer>
				</article>
			<?php endwhile; ?>

			<?php endif; ?>


	</div>
</div>
<?php get_footer();?>
