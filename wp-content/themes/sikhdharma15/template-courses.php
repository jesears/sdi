<?php
/*
 * Template Name: Courses and Classes Template
 */
?>

<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
					<section class="entry-content clearfix" itemprop="articleBody">
						<?php the_content(); ?>
					</section>
					<header class="article-header page-header anchor-links">
						<?php if( have_rows('courses_classes') ): ?>

							<?php $count = 0; ?>
							<?php while ( have_rows('courses_classes') ) : the_row(); ?>
								<?php $count++; ?>
								<?php
								$title = get_sub_field('anchored_title');
								$_title = str_replace(' ', '_', $title);
								?>
								<a href="#<?php echo $_title; ?>"><?php echo $title; ?></a>
							<?php endwhile; ?>
						<?php endif; ?>
					</header>
				</article>
			<?php endwhile; ?>

			<?php endif; ?>
		</div>
	</div>
</div>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<?php if( have_rows('courses_and_classes') ): ?>
		<div class="course-class-container">
			<h2 class="h-course-class">Courses and Classes</h2>
		<?php $count = 0; ?>
		<?php while ( have_rows('courses_and_classes') ) : the_row(); ?>
			<?php
				$course_title = get_sub_field('course_class_title');
				$start_date = get_sub_field('course_class_start_date');
				$end_date = get_sub_field('course_class_end_date');
				$course_location = get_sub_field('course_class_location');
				$image = get_sub_field('course_class_image');
			?>

			<?php if (0 == $count % 2) { $add_class = 'odd'; } else {$add_class = 'even';} ?>
				<div class="container <?php echo $add_class; ?>">
					<div class="row">
						<div class="content-section">
							<div class="course-info col-md-6">
								<h4 class="course-info-heading"><?php echo $course_title; ?></h4>
								<span class="date">
									<?php if($start_date && $end_date) { ?>
										<?php echo date('F j', strtotime($start_date)); ?> - <?php echo date('j, Y', strtotime($end_date)); ?>
									<?php } else { ?>
										<?php echo date('F j, Y', strtotime($start_date)); ?>
									<?php } ?>
								</span>
								<span class="location">
									<?php echo $course_location; ?>
								</span>
								<span class="course-description">
									<?php the_sub_field('course_class_description'); ?>
								</span>
							</div>

							<div class="course-img col-md-offset-1 col-md-4">
								<?php if( !empty($image) ): ?>
									<?php
									$img_src = mapi_thumb(
										array(
											'src' => $image['url'],
											'w'   => 300,
											'h'   => 300,
											'q'   => 90,
											'a'   => 'c',
										)
									);
									?>

									<img src="<?php echo $img_src; ?>" alt="<?php echo $image['alt']; ?>" />

								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php $count++; ?>
		<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<!-- End Anchored Page Section -->

<?php endwhile; ?>
<?php endif; ?>
<div class="container">

	<?php include_once('inc/join-email.php'); ?>
	<article>
		<footer class="article-footer">
			<?php
			wp_link_pages(
					array(
							'next_or_number' => 'number',
							'nextpagelink' => __('Next page', 'blankout'),
							'previouspagelink' => __('Previous page', 'blankout'),
							'pagelink' => '%',
							'echo' => 1
					)
			);
			?>
			<?php if(function_exists('mapi_edit_link')) {
				echo mapi_edit_link();
			} ?>
		</footer>
	</article>

</div>
<?php get_footer(); ?>
