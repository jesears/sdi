<?php
/**
 * Template Name: Jobs
 */

get_header(); ?>

<div class="container">
    <div class="row">
        <div id="main" class="col-md-9">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> itemscope itemtype="http://schema.org/BlogPosting">

                    <section class="entry-content clearfix" itemprop="articleBody">
                        <?php the_content(); ?>
                    </section>

                    <footer class="article-footer">
                        <?php if (function_exists('mapi_edit_link')) {
                            echo mapi_edit_link();
                        } ?>
                    </footer>
                </article>

            <?php endwhile; ?>

            <?php endif; ?>
        </div>
        <?php get_sidebar(); ?>

    </div>
</div>

<?php get_footer(); ?>
