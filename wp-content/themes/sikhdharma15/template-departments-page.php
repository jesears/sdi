<?php
/*
 * Template Name: Departments
 */
?>

<?php get_header(); ?>

<div class="container">
	<div class="row">
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
				<header class="article-header page-header anchor-links">
					<?php if( have_rows('department_staff') ): ?>
						<?php
							$anchor_section = array();
							$anchor_staff = array();
						?>
						<?php $count = 0; ?>
						<?php while ( have_rows('department_staff') ) : the_row(); ?>
							<?php $count++; ?>
							<?php
								$title = get_sub_field('department_title');
								$_title = str_replace(' ', '_', $title);
							?>

							<?php array_push($anchor_section, '<a href="#' . $_title .'">' . $title . '</a>'); ?>


							<?php if( have_rows('department_staff_repeater') ): ?>
								<?php while ( have_rows('department_staff_repeater') ) : the_row(); ?>
									<?php
									$staff_title = get_sub_field('staff_title');
									$_staff_title = str_replace(' ', '_', $staff_title);
									?>
									<?php array_push($anchor_staff, '<a href="#' . $_staff_title .'">' . $staff_title . '</a>'); ?>
								<?php endwhile; ?>
							<?php endif; ?>

						<?php endwhile; ?>

						<div class="top-anchor-section">
							<?php
								foreach ($anchor_section as $anchor) {
									echo $anchor;
								}
							?>
						</div>
						<?php if(get_field('dept_bookmark_links') == 'yes') : ?>
							<div class="bottom-anchor-section">
								<?php
								foreach ($anchor_staff as $anchor) {
									echo $anchor;
								}
								?>
							</div>
						<?php endif; ?>

					<?php endif; ?>

				</header>
				<section class="entry-content clearfix" itemprop="articleBody">
					<?php the_content(); ?>
				</section>

			</article>
		<?php endwhile; ?>

		<?php endif; ?>
	</div>
</div>


<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<!-- Anchored Page Section -->
	<?php if( have_rows('department_staff') ): ?>

	<?php while ( have_rows('department_staff') ) : the_row(); ?>
		<?php
			$title = get_sub_field('department_title');
			$_title = str_replace(' ', '_', $title);
			$department_content = get_sub_field('department_content');
		?>

			<div class="container department-section">
				<div class="department-section-title">
					<a id="<?php echo $_title; ?>" class="anchor"></a>
					<h2><?php echo $title; ?></h2>
				</div>
				<div class="container">
					<section class="content-section">
						<?php echo $department_content; ?>
						<?php if( have_rows('department_staff_repeater') ): ?>

							<div class="row">

								<?php while( have_rows('department_staff_repeater') ): the_row();
									// vars
									$staff_title = get_sub_field('staff_title');
									$staff_content = get_sub_field('staff_content');
									$staff_image = get_sub_field('staff_image');

									$_staff_title = str_replace(' ', '_', $staff_title);
									?>

									<div class="col-md-12 staff-member">
										<a id="<?php echo $_staff_title; ?>" class="anchor"></a>
										<h2><?php echo $staff_title; ?></h2>
										<div class="staff-content">
											<?php if( $staff_image ): ?>
												<img src="<?php echo $staff_image['url']; ?>" alt="<?php echo $staff_image['alt'] ?>" class="staff-img" />
											<?php endif; ?>
											<?php echo $staff_content; ?>
										</div>
									</div>


								<?php endwhile; ?>

							</div>

						<?php endif; ?>
					</section>
				</div>
			</div>


	<?php endwhile; ?>
<?php endif; ?>
<!-- End Anchored Page Section -->

<?php endwhile; ?>
<?php endif; ?>

<div class="container">

	<?php include_once('inc/join-email.php'); ?>

	<footer class="article-footer">
		<?php if(function_exists('mapi_edit_link')) {
			echo mapi_edit_link();
		} ?>
	</footer>
</div>
<?php get_footer(); ?>
