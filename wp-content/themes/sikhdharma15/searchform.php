<?php $placeholder = esc_attr__('Search', 'blankout'); ?>
<form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>" class="input-group">
	<?php if(get_search_query() == '') : ?>
		<input type="text" value="" name="s" id="s" placeholder="<?php echo $placeholder; ?>" class="form-control" />
	<?php else : ?>
		<input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="<?php the_search_query(); ?>" class="form-control" />
	<?php endif; ?>

	<?php // no submit button version <input type="hidden" id="searchsubmit" /> ?>
</form>