<div class="container">
	<div class="row">
		<?php include_once('inc/join-email.php'); ?>
	</div>
	<footer class="footer footer-container" role="contentinfo">
		<nav role="navigation">
			<div class="row">
				<?php if(is_active_sidebar('footer-widgets')) : dynamic_sidebar('footer-widgets'); endif; ?>
				<div class="social col-lg-4">
					<div class="donation">
						<a href="http://weblink.donorperfect.com/DasvandhSDI">Make a Donation</a>
					</div>
					<a class="mapi-social-link facebook" rel="nofollow" data-placement="top" target="_blank" title="Share this page on Facebook" href="<?php mapi_option("facebook_uri"); ?>"><i class="fa fa-facebook">&nbsp;</i><span>Stay Connected</span></a>
				</div>
			</div>


			<div class="copyright">

				&copy;<?php echo date('Y'); ?> <a href="<?php echo home_url('/') ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>. <?php _e('All rights reserved', 'blankout'); ?>.
				<div class="pull-right text-center-xs">
					<?php blankout_footer_credit(); ?>
				</div>
			</div>
	</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>

