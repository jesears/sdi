<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div id="main" class="col-lg-9">
			<?php if (have_posts()) : ?>
				<h1 class="archive-title"><span><?php _e('Search Results for', 'blankout'); ?>:</span> <?php the_search_query(); ?></h1>

				<?php while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" class="row" <?php post_class('clearfix'); ?>>
					<div class="col-sm-9 col-sm-push-3">
						<header class="article-header post-header">
							<h2 class="h3 entry-title text-uppercase"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
							<small class="byline vcard"><?php _e("Posted", 'blankout'); ?>
								<time class="updated" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time(get_option('date_format', 'l, F j, Y')); ?></time> <?php _e("by", 'blankout'); ?>
								<span class="author"><?php the_author_posts_link(); ?></span> <span class="amp">&amp;</span> <?php _e("filed under", 'blankout'); ?>
								<?php
										if (get_post_type() == "tribe_events") {
											echo get_the_term_list(get_the_ID(), 'tribe_events_cat', '', ', ');
										} else {
											echo get_the_term_list(get_the_ID(), 'category', '', ', ');
										}

								?>
							</small>
						</header>
						<section class="entry-content">
							<?php // the_content(mapi_excerpt_more()); // for use with More Tag ?>
							<?php 
								if (mapi_excerpt()) {
									echo mapi_excerpt() . '<span>&hellip;</span>';
								}
							?>	
						</section>
					</div>
					<section class="entry-image col-sm-3 col-sm-pull-9" style="padding-top:1.25em;">
						<?php if (has_post_thumbnail()) : ?>
							<?php
							mapi_featured_img(
								array(
									'w'     => '200',
									'h'     => '200',
									'class' => 'img-responsive center-block',
								)
							);
							?>
						<?php endif; ?>					
					</section>
					<footer class="article-footer">
						<div class="col-sm-9 col-sm-push-3">
							<span class="btn btn-primary btn-sm" style="margin-bottom:1em;"><?php echo mapi_excerpt_more(); // for standard Mindshare excerpts in mapi-utility.php ?></span>
						</div>
						<div class="col-sm-3 col-sm-pull-9"><?php echo mapi_edit_link(); ?>&nbsp;</div>
						<?php //the_taxonomies('before=<p class="tags">&after=</p>&template=%s: %l'); ?>
						<div class="col-xs-12"><hr class="" style="height:7px; background-color:#aeb0c6;" /></div>
					</footer>

				</article>

				<?php endwhile; ?>

				<?php if (function_exists('blankout_page_nav')) : ?>
					<?php blankout_page_nav(); ?>
				<?php else : ?>
					<nav class="wp-prev-next">
						<ul class="clearfix">
							<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', 'blankout')) ?></li>
							<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', 'blankout')) ?></li>
						</ul>
					</nav>
				<?php endif; ?>

			<?php else : ?>
				<h1 class="archive-title"><span><?php _e('Search Results for', 'blankout'); ?>:</span> <?php the_search_query(); ?></h1>
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
					<header class="article-header page-header">
						<h4 class="entry-title page-title" itemprop="headline"><?php _e('Try a different search term?', 'blankout'); ?></h4>
					</header>
					<section class="entry-content">
						<?php get_search_form(); ?>
					</section>
					<footer class="article-footer"></footer>
				</article>

			<?php endif; ?>
		</div>

		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
