<?php get_header(); ?>
<div class="container home-container">
	<div class="row">
		<div id="main" class="col-md-12">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> itemscope itemtype="http://schema.org/BlogPosting">
					<header class="article-header post-header">
						<?php blankout_rich_snippets(); ?>
					</header>
					<section class="entry-content clearfix" itemprop="articleBody">
						<?php include (get_stylesheet_directory()).'/inc/landing-page-links.php'; ?>
					</section>
				</article>

			<?php endwhile; ?>

			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
