<?php if(have_rows('member_section')): ?>
    <?php while(have_rows('member_section')) : the_row(); ?>
        <?php
        $title = get_sub_field('member_section_title');
        $_title = str_replace(' ', '_', $title);
        ?>
        <div class="board-members">
            <div class="member-title">
                <a id="<?php echo $_title; ?>" class="anchor"></a>
                <h2 class="h-anchor-link"><?php echo $title; ?></h2>
            </div>
            <div class="container">
                <section class="content-section">
                    <?php the_sub_field('member_section_content'); ?>
                </section>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>