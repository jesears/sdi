<?php
/**
 * carousel.php
 *
 * @created   12/18/12 3:53 PM
 * @author    Mindshare Studios, Inc.
 * @copyright Copyright (c) 2013
 * @link      http://www.mindsharelabs.com/documentation/
 *
 */

$slide_query = new WP_Query('post_type=carousel');

if ($slide_query->have_posts()) : ?>

	<div id="carousel" class="flexslider">
		<ul class="slides">

			<?php while ($slide_query->have_posts()) : $slide_query->the_post(); ?>
				<?php if (has_post_thumbnail() && function_exists('mapi_thumb')) : ?>
					<li class="item">
						<img src="<?php echo mapi_thumb(mapi_get_attachment_image_src(), 1170, 400, 90); ?>" class="attachment-full wp-post-image" alt="<?php echo mapi_get_attachment_image_title(); ?>" />

						<?php if (function_exists('mapi_excerpt') && (mapi_excerpt() != '')) : ?>

							<?php
							$cap_link = get_field('caption_link');
							$cap_pos = get_field('caption_position');

							if ($cap_pos == 'left') {
								$pos_class = "text-align: left !important;";
							} else if ($cap_pos == 'right') {
								$pos_class = "text-align:right !important;";
							} else {
								$pos_class = "";
							}

							$cap_color = get_field('caption_title_color');

							if($cap_color == 'white') {
								$cap_style = 'color: rgb(255, 255, 255)';
							} else {
								$cap_style = 'color:rgb(46, 46, 127)';
							}
							?>
							<style>.flex-caption p {text-align:inherit !important;}</style>
							<div style="<?php echo $pos_class; echo $cap_style; ?>" class="flex-caption hidden-sm hidden-xs">
								<?php if($cap_link): ?>
									<a href="<?php echo $cap_link ?>"><p class="flex-caption-title"><?php the_title(); ?></p></a>
								<?php else: ?>
									<p class="flex-caption-title"><?php the_title(); ?></p>
								<?php endif; ?>

								<?php the_content(); ?>
							</div>
						<?php endif; ?>
					</li>
				<?php endif; ?>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</ul>
	</div>
<?php endif; ?>
