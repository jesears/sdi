<?php
	if(!is_page('Cart') ) {
		include(get_stylesheet_directory() . '/inc/breadcrumb.php');
	}
?>
<?php if(get_field('banner_image') || get_field('carousel_slider')) : ?>
	<?php include(get_stylesheet_directory() . '/inc/image-alignment.php'); ?>
		<div id="carousel" class="flexslider">
			<?php if(get_field('single_or_slider') == 'carousel') : ?>
				<?php $slide_query = get_field('carousel_slider'); ?>
				<ul class="slides">
					<?php foreach( $slide_query as $image ): ?>

						<?php
						$img_src = mapi_thumb(
							array(
								'src' => $image['url'],
								'w'   => 1170,
								'h'   => 400,
								'q'   => 90,
								'a'   => 't',
								'zc'  => 1
							)
						);
						?><?php //mapi_var_dump($image); ?>
						<li class="item">
							<img src="<?php echo $img_src; ?>" class="attachment-full wp-post-image" alt="<?php the_title(); ?>" />
							<div class="flex-caption hidden-sm hidden-xs">
								<p class="flex-caption-title"><?php echo $image['description'];  ?></p>
								<p><?php echo mapi_get_attachment_image_caption($image['ID']); ?></p>
							</div>
						</li>

					<?php endforeach; ?>
				</ul>

			<?php else : ?>
				<?php $slide_query = get_field('banner_image'); ?>
			<?php if (function_exists('mapi_thumb')) : ?>
				<?php
				$img_src = mapi_thumb(
					array(
						'src' => $slide_query,
						'w' => 1170,
						'h' => 400,
						'a' => $align
					)
				);

				?>
				<img src="<?php echo $img_src; ?>" class="attachment-full wp-post-image" alt="<?php echo mapi_get_attachment_image_title(); ?>" />
				<?php if (function_exists('mapi_excerpt') && (mapi_excerpt() != '')) : ?>
					<div class="flex-caption hidden-sm hidden-xs">
						<p class="flex-caption-title"><?php if(get_field('caption_heading')) {the_field('caption_heading');} ?></p>
						<p><?php if(get_field('caption_text')) {the_field('caption_text');} ?></p>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		</div>

<?php else : ?>
		<hr class="invisible bottom-margin">
<?php endif; ?>

