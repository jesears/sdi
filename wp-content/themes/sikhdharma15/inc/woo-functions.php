<?php

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail');
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail');

if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {

    /**
     * Get the product thumbnail for the loop.
     *
     * @subpackage  Loop
     */
    function woocommerce_template_loop_product_thumbnail() {
        echo woocommerce_get_product_thumbnail();
    }
}

if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {

    /**
     * Get the product thumbnail, or the placeholder if not set.
     *
     * @subpackage  Loop
     * @param string $size (default: 'shop_catalog')
     * @param int $deprecated1 Deprecated since WooCommerce 2.0 (default: 0)
     * @param int $deprecated2 Deprecated since WooCommerce 2.0 (default: 0)
     * @return string
     */
    function woocommerce_get_product_thumbnail($size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0)
    {
        global $post;
        if (has_post_thumbnail()) {
            $url = mapi_thumb(array(
                'src' => mapi_get_attachment_image_src(),
                'w' => 200,
                'h' => 300,
                'zc' => 2,
                'a' => 'tc',
                'cc' => 'ffffff'
            ));

            return '<img width="300" height="140" src="' . $url . '" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="' . get_bloginfo('name') . '" />';

            //return get_the_post_thumbnail( $post->ID, $size );
        } elseif (wc_placeholder_img_src()) {
            return wc_placeholder_img($size);
        }
    };

}


/**
 * this filter hooks into woocommerce advanced messages to give the after content message a container
 */
add_filter('wcam_display_message', 'mindshare_add_container_to_message', 10, 4);

function mindshare_add_container_to_message($store_message, $location_name, $type, $conditions) {

    if ($location_name == 'woocommerce_after_main_content') {
        return '<div class="container">'. $store_message.'</div>';
    } else {
        return $store_message;
    }
}
