<?php
$alignment = get_field('image_alignment');
if(!empty($alignment)) {

	if($alignment === 'top') {
		$align = 't';
	} elseif ($alignment === 'bottom') {
		$align = 'b';
	} else {
		$align = 'c';
	}
} else {
	$align = 'c';
}
?>
