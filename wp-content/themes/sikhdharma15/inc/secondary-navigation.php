<?php $department_choice = get_field('choose_department'); ?>

<?php if(($department_choice != "default")) : ?>

	<?php if($department_choice == 'camp_miri_piri') {

		$department_option = 'camp_miri_piri';

	} elseif ($department_choice == 'dharmic_jobs') {
		$department_option = 'dharmic_jobs';
	} elseif ($department_choice == 'kirtan') {
		$department_option = 'kirtan';
	} elseif ($department_choice == 'dasvandh') {
		$department_option = 'dasvandh';
	} elseif ($department_choice == 'dharmic_education') {
		$department_option = 'dharmic_education';
	} elseif ($department_choice == 'khalsa_council') {
		$department_option = 'khalsa_council';
	} elseif ($department_choice == 'ministry') {
		$department_option = 'ministry';
	} elseif ($department_choice == 'sikhs_feed_people') {
		$department_option = 'sikhs_feed_people';
	} ?>

	<div class="row">
		<!--No secondary logo on the Landing Page template-->
		<?php if(!is_page_template('template-landing-page.php')) : ?>
			<?php if(get_field('add_secondary_logo') == "yes") : ?>
					<?php //mapi_var_dump(get_field('add_secondary_logo')); ?>
				<div class="col-lg-12 secondary-logo">
					<?php $logo = get_field($department_option . '_logo', 'options'); ?>
					<?php
					$img_src3 = mapi_thumb(
							array(
									'src' => $logo['url'],
/*									'w'   => 437,
									'h'   => 184,*/
									'w'   => '437',
									'h'   => 'auto',
//									'h'   => '400',
									'q'   => 90,
									'a'   => 'c',
									'zc'  => 1
							)
					);
					?>
					<a href="<?php the_field($department_option . '_logo_link', 'options'); ?>"><img src="<?php echo $img_src3; ?>" alt="<?php echo $logo['alt'] ?>" class="secondary-logo" /></a>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<div class="col-lg-12">
			<nav class="navbar navbar-default secondary-navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".secondary-navbar-collapse">
						<span class="sr-only"><?php _e('Toggle navigation', 'blankout'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse secondary-navbar-collapse">
					<?php
					if(has_nav_menu($department_option . '-nav')) {
						wp_nav_menu(
								array(
										'container'       => ' ',
										'container_class' => 'nav',
										'fallback_cb'     => 'wp_page_menu',
										'menu'            => $department_option . '-nav',
										'menu_class'      => 'nav nav-justified',
										'theme_location'  => $department_option . '-nav',
										'depth'           => '1',
										'walker'          => new Blankout_Menu_Walker()
								)
						);
					} ?>
				</div>
			</nav>
		</div>
	</div>
<?php endif; ?>
