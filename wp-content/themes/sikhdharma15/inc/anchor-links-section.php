<?php if(have_rows('anchored_page_section')): ?>
    <?php $count = 0; ?>
    <?php while(have_rows('anchored_page_section')) : the_row(); ?>
        <?php
        $title = get_sub_field('anchored_title');
        $_title = str_replace(' ', '_', $title);
        ?>
        <?php if (0 == $count % 2) { $add_class = 'odd'; } else {$add_class = 'even';} ?>
            <div class="anchor-links-layout <?php echo $add_class; ?>">
                <div class="container">
                    <a id="<?php echo $_title; ?>" class="anchor"></a>
                    <h2 class="h-anchor-link"><?php echo $title; ?></h2>
                    <section class="content-section">
                        <?php the_sub_field('anchored_content'); ?>
                        <?php if( have_rows('add_images') ): ?>
                            <div class="row">
                                <?php $image_count = 0; ?>
                                <?php while( have_rows('add_images') ): the_row();
                                    $image_heading = get_sub_field('image_heading');
                                    $image = get_sub_field('content_image');
                                    ?>
                                    <div class="col-sm-6 col-lg-4 col-xs-12">
                                        <?php if( $image ): ?>
                                            <h4><?php echo $image_heading; ?></h4>
                                            <?php
                                            $extra_img_src = mapi_thumb(
                                                array(
                                                    'src' => $image['url'],
                                                    'w'   => 300,
                                                    'h'   => 250,
                                                    'q'   => 90,
                                                    'a'   => 'c',
                                                    'zc'  => 1
                                                )
                                            );
                                            ?>

                                            <img src="<?php echo $extra_img_src; ?>" alt="<?php echo $image['alt'] ?>" class="anchored-img" />
                                        <?php endif; ?>
                                    </div>

                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </section>
                </div>
            </div>
        <?php $count++; ?>
    <?php endwhile; ?>
<?php endif; ?>
