<?php $i = 0 ?>
<?php if(have_rows('gallery')) : ?>
	<?php while(have_rows('gallery')) : the_row();
		$i++;
		$image_or_video = get_sub_field('image_or_video');
		$image = get_sub_field('image');
		$video = get_sub_field('video');
		$heading = get_sub_field('heading');
		$sub_heading = get_sub_field('sub_heading');
		$link = get_sub_field('link_to');
		$rows = get_field('gallery');

	?>
		<?php
		$l_alignment = get_sub_field('image_alignment_landing');
		if(!empty($l_alignment)) {

			if($l_alignment === 'top') {
				$l_align = 't';
			} elseif ($l_alignment === 'bottom') {
				$l_align = 'b';
			} else {
				$l_align = 'c';
			}
		} else {
			$l_align = 'c';
		}
		?>

		<?php if (!$active_row): $active_row = true; ?>
			<div class="row">
		<?php endif; ?>


		<div class="col-sm-6 col-xs-12 landing-gallery">
		<?php if($image_or_video == 'image') : ?>

			<?php
			$img_src = mapi_thumb(
				array(
					'src' => $image['url'],
					'w'   => 585,
					'h'   => 330, 
					'q'   => 90,
					'a'   => $l_align
				)
			);
			?>
		<?php endif; ?>

			<?php if ($image_or_video == 'image'): ?>

				<a href="<?php echo $link; ?>">
				<img src="<?php echo $img_src; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			<?php else : ?>
				<?php $embed_code = wp_oembed_get($video);
					//mapi_var_dump($embed_code);
				?>
				<div class="video-container">
					<?php /*echo do_shortcode('[embed]'.$video.'[/embed]') */?>
					<?php echo $embed_code; ?>
				</div>
			<?php endif; ?>

		<?php if($link) : ?><a href="<?php echo $link; ?>"><?php endif; ?>
				<p class="heading"><?php echo $heading; ?></p>
		<?php if($link) : ?></a><?php endif; ?>
		<?php if(!empty($sub_heading)) : ?>
			<p class="subheading"><?php echo $sub_heading; ?></p>
		<?php else: ?>
			<br /><br />
		<?php endif; ?>
	</div>
	<?php if ($active_row && ($i % 2 == 0 )): $active_row = false; ?>
		</div>
	<?php endif; ?>
	<?php endwhile; ?>

	<!--close row if still active-->
	<?php if ($active_row):?>
		</div>
	<?php endif; ?>
<?php endif; ?>
