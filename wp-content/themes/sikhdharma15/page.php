<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div id="main" class="col-md-12">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php if( have_rows('anchored_page_section') ): ?>
					<?php if(get_field('bookmark_links') == 'yes') : ?>
					<div class="article-header page-header anchor-links">
						<?php $count = 0; ?>
						<?php while ( have_rows('anchored_page_section') ) : the_row(); ?>
							<?php $count++; ?>
							<?php
							$title = get_sub_field('anchored_title');
							$_title = str_replace(' ', '_', $title);
							?>
							<a href="#<?php echo $_title; ?>"><?php echo $title; ?></a>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
				<?php endif; ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> itemscope itemtype="http://schema.org/BlogPosting">
					<section class="entry-content clearfix" itemprop="articleBody">
						<?php the_content(); ?>
					</section>

					<?php include(get_stylesheet_directory() . '/inc/anchor-links-section.php'); ?>

					<?php include(get_stylesheet_directory() . '/inc/board-members.php'); ?>

					<footer class="article-footer">
						<?php if (function_exists('mapi_edit_link')) {
							echo mapi_edit_link();
						} ?>
					</footer>
				</article>

			<?php endwhile; ?>

			<?php endif; ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>
