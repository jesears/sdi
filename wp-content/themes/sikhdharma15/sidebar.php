<div id="sidebar" class="col-md-3">
	<?php

	if (is_active_sidebar('blog-sidebar') && (is_archive() || is_singular('post') || is_home())) {

		dynamic_sidebar('blog-sidebar');
	} elseif (is_active_sidebar('main-sidebar') && !is_page_template('template-jobs.php')) {

		dynamic_sidebar('main-sidebar');
	} elseif (is_active_sidebar('jobs-sidebar') && (is_page_template('template-jobs.php'))) {

		dynamic_sidebar('jobs-sidebar');
	} ?>
</div>
